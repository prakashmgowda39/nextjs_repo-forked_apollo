interface IButtonProps {
    onClick?: () => void;
    label: string;
}

export const Button = (props: IButtonProps) => {
    return (
        <button className="btn btn-basic">
            {props.label}
        </button>
    )
}