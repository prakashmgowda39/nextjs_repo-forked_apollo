import { SearchCard } from './search-card';

const TopTransperantHeader = (props) => (
    <div className="top-header">
        <nav className="navbar navbar-default">
            <div className="container-fluid">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                    <a className="navbar-brand">
                        <img src={`/static/images/logo.png`} />
                    </a>
                </div>
                <ul className="nav navbar-nav">
                    <li className="active"><a href="#">Parents </a></li>
                    <li className="active"><a href="#">Students </a></li>
                    <li className="active"><a href="#">Providers </a></li>
                    <li className="active"><a href="#">Affilates </a></li>
                    <li className="active"><a href="#">Blog </a></li>
                </ul>
            </div>
        </nav>
    </div>
)


export const TopSection = (props) => (
    <div className="top-section">
        <TopTransperantHeader />
        <div className="top-section-content">
            <SearchCard placeholder="Try “Yoga near me”…"/>
        </div>
    </div>
);